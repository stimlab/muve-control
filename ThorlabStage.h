#pragma once
#include <string>
#include <queue>
#include <fstream>

struct MoveCommand {
	int axis;
	double position;
	double velocity;
	double acceleration;
};

class ThorlabStage {

	std::string m_serial_num;
	bool m_moving[3] = { false, false, false };
	const int m_polling_rate = 200;
	const double dev2real = 0.000002441406247;
	const double real2dev = 1.0 / dev2real;
	std::queue<MoveCommand> m_command_queue;
	void ProcessCommandQueue();
	std::ofstream m_logstream;
	bool m_logging = false;
	bool m_active[3] = { false, false, false };	// flag whether each stage is active

	std::string timestamp();
	std::string number(double d);


public:

	void Init();
	void Destroy();
	void UpdateStatus();
	void Enable();
	void Disable();
	void StartLog(std::string logfile = "stagelog.txt");
	void StopLog();

	// stage status functions
	bool isMoving();
	bool Logging();

	double getPosition(int axis);
	double getVelocity(int axis);
	void setVelocity(int axis, double velocity);
	double getAcceleration(int axis);
	void setAcceleration(int axis, double acceleration);
	size_t getCommandQueueSize();

	void MoveToX(double x);
	void MoveToY(double y);
	void MoveToZ(double z);
	void MoveTo(double x, double y, double z);
	void Home(int axis);

	void QueueMove(int axis, double position, double velocity, double acceleration);
};