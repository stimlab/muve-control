#include <gl/glew.h>
#include "muve_control.h"
#include "ThorlabStage.h"
#include "ThorlabCamera.h"
#include <iostream>
#include <string>
#include <GLFW/glfw3.h> // Will drag system OpenGL headers
#include <thorcam/tl_camera_sdk.h>
#include <thorcam/tl_camera_sdk_load.h>
#include "tira/image/colormap.h"
#include "muve.h"
#include "tira/graphics_gl.h"

MUVE_Microscope muve;

tira::glShader camera_image_shader;
tira::glGeometry rectangle;

std::vector<MuveImageTexture> texture_list;

extern void SaveParameters();

/// <summary>
/// Add a texture map from the MUVE image queue to the GUI texture list
/// </summary>
void UpdateTextures() {

    if (texture_list.size() > 0 && muve.getImageQueueSize() == 0)               // if the MUVE image queue has been emptied
        texture_list.clear();                                                   // also clear the texture queue

    if (texture_list.size() == muve.getImageQueueSize() - 1) {                  // if the MUVE image queue has one more image than the texture queue
        size_t num_images = muve.getImageQueueSize();                           // get the image queue size
        MUVE_ImageData new_image = muve.getCollectedImage(num_images - 1);      // retrieve the most recent image
        MuveImageTexture new_texture;                                           // create a new texture map and assign its location
        new_texture.pos[0] = new_image.x;
        new_texture.pos[1] = new_image.y;
        new_texture.tex.AssignImage(new_image.image.data(), new_image.image.width(), new_image.image.height(), 0, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
        texture_list.push_back(new_texture);
    }

}

// Clear the mosaic by destroying all of the textures and emptying the vector
void ClearMosaic() {
    for (size_t i = 0; i < texture_list.size(); i++)
        texture_list[i].tex.Destroy();
    texture_list.clear();
}

// GUI parameters
float ui_scale = 1.5f;
bool show_demo_window = true;
ImVec4 clear_color = ImVec4(0.2f, 0.2f, 0.2f, 1.00f);
int display_w, display_h;
bool mouse_dragging = false;
double mouse_pos[2];
float render_pos[2] = { 0.0, 0.0 };                                     // camera position (center of the screen) in millimeters
float render_fov = 10;                                                  // camera fov in millimeters
float render_fov_step = 0.1;                                            // camera zoom step (as a fraction of the current fov)

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
        mouse_dragging = true;
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
        mouse_dragging = false;
}
static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {
    if (mouse_dragging) {
        double distance[2] = { mouse_pos[0] - xpos , mouse_pos[1] - ypos };
        double pixelstep = render_fov / (double)display_h;
        render_pos[0] -= distance[1] * pixelstep;
        render_pos[1] += distance[0] * pixelstep;
    }
    mouse_pos[0] = xpos;
    mouse_pos[1] = ypos;
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    render_fov -= yoffset * render_fov_step * render_fov;
}

int main(int argc, char** argv)
{

    // Initializes camera dll
    if (tl_camera_sdk_dll_initialize())
        std::cout << "FAILED TO LOAD CAMERA DLL" << std::endl;

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    

    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1600, 1200, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    //set input callbacks
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetScrollCallback(window, scroll_callback);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        printf("Error: %s\n", glewGetErrorString(err));
    }
    printf("Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

    camera_image_shader.LoadShader("camera.shader");
    rectangle = tira::glGeometry::GenerateRectangle<float>();

    muve.Init();
    //camera.Init();                              // initialize the camera
    //muve.camera.Snap();
    //tira::image<unsigned char> image = muve.camera.Image8();
    //image.save("image-8bit.bmp");
    //stage.Init();                               // initialize the stage
    InitGUI(window, ui_scale, glsl_version);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();                                           // poll any input events

        muve.ProcessCommandQueue();                                 // update the microscope status and process the command queue

        
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);

        // Render the mosaic here
        float aspect_ratio = (float)display_w / (float)display_h;   // calculate the aspect ratio
        float view_height = render_fov;                             // initialize the view height (in millimeters)
        float view_width = view_height * aspect_ratio;              // calculate the view width (in millimeters) from the aspect ratio

        glm::mat4 projection = glm::ortho(-view_width / 2.0f, view_width / 2.0f, -view_height / 2.0f, view_height / 2.0f, 0.0f, 100.0f);
        glm::mat4 view = glm::lookAt(glm::vec3(render_pos[0], render_pos[1], -1.0f), glm::vec3(render_pos[0], render_pos[1], 0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        
        UpdateTextures();                                           // update textures from the MUVE image queue
        camera_image_shader.Bind();
        for (size_t t = 0; t < texture_list.size(); t++) {
            texture_list[t].tex.Bind();
            glm::mat4 model = glm::mat4(1.0);
            model = glm::translate(model, glm::vec3((float)(texture_list[t].pos[0]), (float)(texture_list[t].pos[1]), 0.0f));
            double fov_x, fov_y;
            muve.getFov(fov_x, fov_y);
            model = glm::scale(model, glm::vec3((float)fov_x, (float)fov_y, 1.0));
            glm::mat4 mvp = projection * view * model;
            camera_image_shader.SetUniformMat4f("MVP", mvp);
            rectangle.Draw();
        }       

        // Iterate through all stored textures and draw them to the screen
        /*MUVE_ImageData image;
        if (muve.PopImage(image)) {
            if (image.index_x == 0 && image.index_y == 0)               // If the image indices are (0, 0), then we are starting a new image
                ClearMosaic();                                          // so clear all current textures.
            MuveImageTexture new_texture;
            new_texture.pos[0] = image.x;
            new_texture.pos[1] = image.y;
            new_texture.tex.AssignImage(image.image.data(), image.image.width(), image.image.height(), 0, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
            texture_list.push_back(new_texture);
        }*/

        RenderGUI();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }
    SaveParameters();

    muve.Destroy();

    DestroyGUI();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}