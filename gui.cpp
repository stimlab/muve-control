#include "muve_control.h"
#include "muve.h"
#include <GLFW/glfw3.h> // Will drag system OpenGL headers
#include "ImGuiFileDialog/ImGuiFileDialog.h"

#include <fstream>
#include <ctime>
#include <sstream>

extern MUVE_Microscope muve;

// testing and setup parameters
double cut_plane = 58.9;
double cut_line = 10;
bool cut_lock = true;
double under_knife = 17;
bool under_knife_lock = true;
double under_objective = 110;

bool under_objective_lock = true;

// stage limits
double x_min = 0.0;
double x_max = 150.0;
double y_min = 0.0;
double y_max = 150.0;
double z_min = 0;
double z_max = 150.0;
double home[3] = { 150.0, 150.0, 150.0 };
double current_position[3];
double destination[3];

// cutting parameters
bool system_lock = true;
double cut_length = 100;
double section_thickness = 1;
unsigned int cut_sections = 10;
double small_cut_step = 0.1;
double big_cut_step = 1;
double step_slow = 0.1;
double step_fast = 1;
bool cutting_param_lock = true;
double sample_thickness = 3;

// imaging parameters
double fov[2];
double exposure_ms;                                             // camera exposure time
double scan_start[2];                                           // mosaic start position
double scan_size[2];                                            // size of the mosaic (in spatial coordinates)
unsigned int scan_frames[2];
double overlap_percent;                                         // overlap percentage
extern std::vector<MuveImageTexture> texture_list;
extern float render_pos[2];
extern float render_fov;
float acquire_position[] = { 0, 0 };
std::string imageRepository;
std::string sampleName;
char sample_note[256];
int section = 0;
int scan_sections = 10;

size_t start_queue_size = 0;


std::string parameterFile = "muve_parameters.txt";
void SaveParameters() {
    std::ofstream outfile(parameterFile);

    outfile << imageRepository << std::endl;
    outfile << sample_thickness << std::endl;
    outfile << overlap_percent << std::endl;
    outfile << scan_start[0] << std::endl;
    outfile << scan_start[1] << std::endl;
    outfile << scan_frames[0] << std::endl;
    outfile << scan_frames[1] << std::endl;
    outfile << sample_note << std::endl;

    outfile.close();
}

void LoadParameters() {
    std::ifstream infile(parameterFile);
    if (!infile)                                                // if the file doesn't exist
        return;                                                 // return (using default values)

    char repo_name[256];
    infile.getline(repo_name, 256);
    imageRepository = std::string(repo_name);
    infile >> sample_thickness;
    muve.setSampleThickness(sample_thickness);
    infile >> overlap_percent;
    muve.setOverlapPercent(overlap_percent);
    infile >> scan_start[0];
    infile >> scan_start[1];
    muve.setScanStart(scan_start[0], scan_start[1]);
    infile >> scan_frames[0];
    infile >> scan_frames[1];
    muve.setScanFrames(scan_frames[0], scan_frames[1]);
    infile.get();
    infile.getline(sample_note, 256);
    infile.close();
}

std::string GenerateSampleName() {
    
    time_t now = time(0);                                                      // current date/time based on current system
    tm* ltm = localtime(&now);                                                 // get a time structure

    std::stringstream ss;                                                           // create a string stream for character processing
    ss << 1900 + ltm->tm_year << "_";
    ss << std::setw(2) << std::setfill('0') << ltm->tm_mon + 1 << "_";
    ss << std::setw(2) << std::setfill('0') << ltm->tm_mday << "_";
    ss << std::setw(2) << std::setfill('0') << ltm->tm_hour << "_";
    ss << std::setw(2) << std::setfill('0') << ltm->tm_min << "_";
    ss << std::setw(1) << std::setfill('0') << ltm->tm_sec;

    return ss.str();
}

void NewSample() {
    sampleName = GenerateSampleName();
    std::string dir = imageRepository + "\\" + sampleName + "_" + std::string(sample_note);
    muve.setImageDirectory(dir);
}

void SetDestination(double x, double y, double z) {
    destination[0] = x;
    destination[1] = y;
    destination[2] = z;
}
void MoveStage() {
    muve.stage.MoveTo(destination[0], destination[1], destination[2]);
}
void MoveStage(double x, double y, double z) {
    SetDestination(x, y, z);
    MoveStage();
}
void MoveToKnife() {
    MoveStage(under_knife, cut_line, cut_plane + sample_thickness);
}
void MoveToObjective() {
    muve.toObjective();
}

/// <summary>
/// Queues up a section cut (assuming that the sample is currently at the objective position
/// </summary>
void QueueSection() {

    muve.Cut();
}

void QueueSections(unsigned int sections) {
    for (unsigned int i = 0; i < sections; i++) {
        QueueSection();
    }
}

void InitGUI(GLFWwindow* window, float ui_scale, const char* glsl_version) {

    // initialize the current stage position
    SetDestination(muve.stage.getPosition(1), muve.stage.getPosition(2), muve.stage.getPosition(3));

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
 
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    ImGui::GetStyle().ScaleAllSizes(ui_scale);
    ImGui::GetIO().FontGlobalScale = ui_scale;

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    io.Fonts->AddFontFromFileTTF("Roboto-Medium.ttf", ui_scale * 16.0f);

    // get MUVE instrument data
    muve.getScanStart(scan_start[0], scan_start[1]);
    muve.getScanSize(scan_size[0], scan_size[1]);
    muve.getScanFrames(scan_frames[0], scan_frames[1]);
    muve.getFov(fov[0], fov[1]);
    overlap_percent = muve.getOverlapPercent();
    overlap_percent *= 100;

    LoadParameters();
    NewSample();
}



void PushHue(double hue) {
    ImGui::PushStyleColor(ImGuiCol_FrameBg, (ImVec4)ImColor::HSV(hue, 0.5f, 0.5f));
    ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, (ImVec4)ImColor::HSV(hue, 0.6f, 0.5f));
    ImGui::PushStyleColor(ImGuiCol_FrameBgActive, (ImVec4)ImColor::HSV(hue, 0.7f, 0.5f));
    ImGui::PushStyleColor(ImGuiCol_SliderGrab, (ImVec4)ImColor::HSV(hue, 0.9f, 0.9f));
}

void PopHue() {
    ImGui::PopStyleColor(4);
}
void RenderGUI() {
    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    {
        static float f = 0.0f;
        static int counter = 0;

        bool muve_active = muve.isActive();
        double max_velocity[3];
        double max_acceleration[3];
        double cut_velocity = muve.getCutVelocity();

        muve.getMaxVelocity(max_velocity[0], max_velocity[1], max_velocity[2]);
        muve.getMaxAcceleration(max_acceleration[0], max_acceleration[1], max_acceleration[2]);
        double overlap_ratio;
        muve.getFov(fov[0], fov[1]);
        overlap_percent = muve.getOverlapPercent();

        // fill the GUI variables with stage parameters
        muve.getPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
        muve.getStageLimits(x_min, x_max, y_min, y_max, z_min, z_max);
        muve.getSampleThickness(sample_thickness);

        ImGui::Begin("MUVE Settings");

        current_position[0] = muve.stage.getPosition(1);
        current_position[1] = muve.stage.getPosition(2);
        current_position[2] = muve.stage.getPosition(3);
        //muve.stage.UpdateStatus();

        if (!muve_active) {
            destination[0] = current_position[0];
            destination[1] = current_position[1];
            destination[2] = current_position[2];
        }

        ImGui::Text("Automated Cutting/Imaging");
        if (muve_active) ImGui::BeginDisabled();
        {
            if (ImGui::Button("Cut")) {
                for (int s = 0; s < cut_sections; s++)
                    muve.Cut();
                start_queue_size = muve.getCommandQueueSize();
            }
            ImGui::SameLine();
            if (ImGui::Button("Scan and Section")) {
                for (int s = 0; s < cut_sections; s++) {
                    muve.QueueScan(true);
                }
                render_pos[0] = scan_start[0];
                render_pos[1] = scan_start[1];
            }
            ImGui::SameLine();
            ImGui::PushItemWidth(0.2 * ImGui::CalcItemWidth());
            std::stringstream ss;
            ss << "sections (" << std::setprecision(2) << 0.001 * cut_sections * section_thickness << " mm)";
            ImGui::InputScalar(ss.str().c_str(), ImGuiDataType_U32, &cut_sections, 0, 0, 0, ImGuiInputTextFlags_EnterReturnsTrue);
            ImGui::PopItemWidth();
        }
        if (muve_active) ImGui::EndDisabled();
        PushHue(0.0);
        if (ImGui::Button("Purge Scans")) {
            muve.PurgeScans();
        }
        PopHue();
        // progress bar for automated queues
        if (start_queue_size > 0) {
            std::stringstream ss;
            float progress = 1.0 - (double)muve.getCommandQueueSize() / (double)start_queue_size;
            ss << muve.getCommandQueueSize() << " / " << start_queue_size << std::fixed << std::setprecision(2) << " (" << progress * 100 << "%)";
            ImGui::ProgressBar(progress, ImVec2(0.f, 0.f), ss.str().c_str());
        }
        if (muve.getCommandQueueSize() == 0) start_queue_size = 0;                      // reset the progress bar when completed

        ImGui::Text("");
        ImGui::Text("Manual Stage Control");
        if (muve_active) ImGui::BeginDisabled(); {
            // ImGui::SliderScalarN("Destination", ImGuiDataType_Double, destination, 3, &x_min, &x_max, "%.4f");
            ImGui::BeginDisabled();
            //PushHue(5.0 / 7.0);
            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            ImGui::SliderScalar("##CurrentX", ImGuiDataType_Double, &current_position[0], &x_min, &x_max, "%.4f");
            ImGui::PopItemWidth();
            //PopHue();
            ImGui::EndDisabled();

            ImGui::SameLine();

            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            PushHue(0.0);
            if (ImGui::InputDouble("X", &destination[0], 0.01, 1.0, "%.4f")) {
                muve.QueueMove(1, destination[0]);
            }
            PopHue();
            ImGui::PopItemWidth();

            ImGui::BeginDisabled();
            //PushHue(5.0 / 7.0);
            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            ImGui::SliderScalar("##CurrentY", ImGuiDataType_Double, &current_position[1], &x_min, &x_max, "%.4f");
            ImGui::PopItemWidth();
            //PopHue();
            ImGui::EndDisabled();

            ImGui::SameLine();

            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            PushHue(0.37);
            if (ImGui::InputDouble("Y", &destination[1], 0.01, 1.0, "%.4f")) {                          // Y axis manual movement
                muve.QueueMove(2, destination[1]);
            }
            PopHue();
            ImGui::PopItemWidth();

            ImGui::BeginDisabled();
            //PushHue(5.0 / 7.0);
            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            ImGui::SliderScalar("##CurrentZ", ImGuiDataType_Double, &current_position[2], &x_min, &x_max, "%.4f");
            ImGui::PopItemWidth();
            //PopHue();
            ImGui::EndDisabled();

            ImGui::SameLine();

            ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
            PushHue(0.69);
            if (ImGui::InputDouble("Z", &destination[2], 0.01, 1.0, "%.4f")) {              // Z axis manual movement
                muve.QueueMove(3, destination[2]);
            }
            PopHue();
            ImGui::PopItemWidth();

            if (ImGui::Button("Home X")) muve.stage.Home(1);
            ImGui::SameLine();
            if (ImGui::Button("Home Y")) muve.stage.Home(2);
            ImGui::SameLine();
            if (ImGui::Button("Home Z")) muve.stage.Home(3);

            //if (ImGui::Button("Return")) MoveStage(home[0], home[1], home[2]);
        }
        if (muve_active) ImGui::EndDisabled();

        if (muve_active) ImGui::BeginDisabled(); {
            if (ImGui::Button("To Knife")) muve.toKnife();
            ImGui::SameLine();
            if (ImGui::Button("To Objective"))
                muve.toObjective();
        }
        if (muve_active) ImGui::EndDisabled();

        if (muve_active) ImGui::BeginDisabled(); {
            if (ImGui::Button("Cut Section")) {
                muve.Cut();
            }

        }
        if (muve_active) ImGui::EndDisabled();

        

        ImGui::Text("");
        ImGui::Text("Image Acquisition");
        if (ImGui::Button("Acquire Here")) {                     // button to acquire the next image in a mosaic
            muve.AcquireImage();
            render_pos[0] = current_position[0];
            render_pos[1] = current_position[1];
        }
        ImGui::SameLine();
        if (ImGui::Button("Clear")) {
            muve.ClearImages();
            texture_list.clear();
        }

        if (ImGui::Button("Acquire At")) {                      // button to acquire the next image in a mosaic
            muve.QueueScan(acquire_position[0], acquire_position[1]);
            render_pos[0] = acquire_position[0];
            render_pos[1] = acquire_position[1];
        }
        ImGui::SameLine();
        ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
        ImGui::InputFloat2("##AcquirePosition", acquire_position);
        ImGui::PopItemWidth();
        if (ImGui::Button("Scan")) {
            //muve.setScanStart(current_position[0], current_position[1]);
            muve.QueueScan();
            render_pos[0] = scan_start[0];
            render_pos[1] = scan_start[1];
        }
        ImGui::SameLine();
        if (ImGui::Button("Scan Here")) {
            muve.QueueScan(current_position[0], current_position[1]);
            render_pos[0] = current_position[0];
            render_pos[1] = current_position[1];
        }
        ImGui::SameLine();
        
        
        ImGui::Text("");
        if (ImGui::BeginTabBar("MUVE Tab Bar")) {
            if (ImGui::BeginTabItem("System")) {
                ImGui::Text("System Settings");
                ImGui::Checkbox("Lock System Settings", &system_lock);
                if (system_lock) ImGui::BeginDisabled(); {
                    if (ImGui::InputScalar("Cut Plane (z)", ImGuiDataType_Double, &cut_plane, &step_slow, &step_fast, "%.2f", ImGuiInputTextFlags_EnterReturnsTrue))
                        muve.setPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
                    if (ImGui::InputScalar("Cut Line (y)", ImGuiDataType_Double, &cut_line, &step_slow, &step_fast, "%.2f", ImGuiInputTextFlags_EnterReturnsTrue))
                        muve.setPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
                    if (ImGui::InputScalar("Objective (x)", ImGuiDataType_Double, &under_objective, &step_slow, &step_fast, "%.2f", ImGuiInputTextFlags_EnterReturnsTrue))
                        muve.setPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
                    if (ImGui::InputScalar("Knife (x)", ImGuiDataType_Double, &under_knife, &step_slow, &step_fast, "%.2f", ImGuiInputTextFlags_EnterReturnsTrue))
                        muve.setPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
                    if (ImGui::InputScalar("Cut Swing", ImGuiDataType_Double, &cut_length, &small_cut_step, &big_cut_step, "%f mm"))
                        muve.setPathParameters(under_objective, under_knife, cut_line, cut_plane, cut_length);
                    if (ImGui::InputScalarN("Velocity", ImGuiDataType_Double, max_velocity, 3, 0, 0, "%.2f mm/s"))
                        muve.setMaxVelocity(max_velocity[0], max_velocity[1], max_velocity[2]);
                    if (ImGui::InputScalarN("Acceleration", ImGuiDataType_Double, max_acceleration, 3, 0, 0, "%.2f mm/s"))
                        muve.setMaxAcceleration(max_acceleration[0], max_acceleration[1], max_acceleration[2]);
                    if (ImGui::InputScalar("Cut Velocity", ImGuiDataType_Double, &cut_velocity, &step_slow, &step_fast, "%.2f mm/s"))
                        muve.setCutVelocity(cut_velocity);
                    if (ImGui::InputScalarN("FOV", ImGuiDataType_Double, fov, 2)) {
                        muve.setFov(fov[0], fov[1]);
                        muve.setOverlapPercent(overlap_percent);
                        muve.getScanSize(scan_size[0], scan_size[1]);
                    }
                }
                if (system_lock) ImGui::EndDisabled();
                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("Sample")) {
                // GUI for manual cutting
                ImGui::Text("Cutting Parameters");
                ImGui::Checkbox("Lock Cutting Parameters", &cutting_param_lock);
                if (cutting_param_lock) ImGui::BeginDisabled();
                if (ImGui::InputScalar("Section", ImGuiDataType_Double, &section_thickness, &small_cut_step, &big_cut_step, "%f um", ImGuiSliderFlags_Logarithmic))
                    muve.setSectionThickness(section_thickness);   

                if (ImGui::InputScalar("Sample", ImGuiDataType_Double, &sample_thickness, &small_cut_step, &big_cut_step, "%f mm"))
                    muve.setSampleThickness(sample_thickness);
                if (ImGui::Button("Set Sample Surface")) {
                    muve.setSampleThickness(muve.stage.getPosition(3) - cut_plane);
                }
                if (cutting_param_lock) ImGui::EndDisabled();

                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("Imaging")) {
                // open Dialog Simple
                if (ImGui::Button("Image Repository"))
                    ImGuiFileDialog::Instance()->OpenDialog("ChooseDirDlgKey", "Choose a Directory", nullptr, ".");

                // display
                if (ImGuiFileDialog::Instance()->Display("ChooseDirDlgKey"))
                {
                    // action if OK
                    if (ImGuiFileDialog::Instance()->IsOk())
                    {
                        //std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                        imageRepository = ImGuiFileDialog::Instance()->GetCurrentPath();
                        SaveParameters();
                    }

                    // close
                    ImGuiFileDialog::Instance()->Close();
                }
                ImGui::Text("Repo: ");
                ImGui::SameLine();
                ImGui::Text(imageRepository.c_str());

                ImGui::Text("Sample: ");
                ImGui::SameLine();
                ImGui::Text(sampleName.c_str());
                ImGui::SameLine();
                ImGui::PushItemWidth(0.5 * ImGui::CalcItemWidth());
                if (ImGui::InputText("##SampleNote", sample_note, 256, ImGuiInputTextFlags_EnterReturnsTrue)) {
                    NewSample();
                }
                ImGui::PopItemWidth();

                ImGui::Text("Section: ");
                ImGui::SameLine();
                char sec_num[100];
                ImGui::Text(itoa(section, sec_num, 10));

                ImGui::Text("Cutting Parameters");
                if (ImGui::InputDouble("Overlap (%)", &overlap_percent)) {
                    muve.setFov(fov[0], fov[1]);
                    muve.setOverlapPercent(overlap_percent);
                    muve.getScanSize(scan_size[0], scan_size[1]);
                }
                exposure_ms = muve.getExposure();
                if (ImGui::InputDouble("Exposure", &exposure_ms, 0.0, 0.0, "%0.1f ms")) {
                    muve.setExposure(exposure_ms);
                }

                ImGui::PushItemWidth(0.82 * ImGui::CalcItemWidth());
                if (ImGui::Button("Here")) {
                    scan_start[0] = current_position[0];
                    scan_start[1] = current_position[1];
                    muve.setScanStart(scan_start[0], scan_start[1]);
                }
                ImGui::SameLine();
                if (ImGui::InputScalarN("Scan Start", ImGuiDataType_Double, scan_start, 2)) {
                    muve.setScanStart(scan_start[0], scan_start[1]);
                }
                ImGui::PopItemWidth();
                muve.getScanSize(scan_size[0], scan_size[1]);
                if (ImGui::InputScalarN("Scan Size", ImGuiDataType_Double, scan_size, 2)) {
                    muve.setScanSize(scan_size[0], scan_size[1]);
                    muve.getScanFrames(scan_frames[0], scan_frames[1]);
                }                             
                if (ImGui::InputScalarN("Scan Frames", ImGuiDataType_U32, scan_frames, 2)) {
                    muve.setScanFrames(scan_frames[0], scan_frames[1]);
                    muve.getScanSize(scan_size[0], scan_size[1]);
                }

                std::stringstream ss;
                ss << "Images in stack: " << texture_list.size();
                ImGui::Text(ss.str().c_str());

                ImGui::EndTabItem();
            }
            ImGui::EndTabBar();
        }

        ImGui::End();
    }

    // Rendering
    ImGui::Render();
}

void DestroyGUI() {
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}