#pragma once

#include <Windows.h>
#include "tira/image/image.h"



class ThorlabCamera {
	void* m_camera_handle = 0;
	bool m_sdk_open = false;
	bool m_mono2color_sdk_open = false;
	bool m_dll_init = false;
	enum TL_CAMERA_SENSOR_TYPE m_camera_sensor_type;
	enum TL_COLOR_FILTER_ARRAY_PHASE m_color_filter_array_phase;
	float m_color_correction_matrix[9];
	float m_default_white_balance_matrix[9];
	int m_bit_depth;
	long long m_exposure;
	int m_gain_range[2];
	double m_gain_dB = 6.0;
	int m_image_size[2];
	unsigned short* m_callback_buffer = 0;
	//unsigned short* m_buffer = 0;
	//tira::image<unsigned short> m_image;

	HANDLE m_frame_acquired_event = 0;
	volatile int m_is_first_frame_finished = 0;
	void* m_mono_to_color_processor_handle = 0;

	void allocate_buffers();



public:

	void setExposure(double milliseconds);
	double getExposure();

	ThorlabCamera();
	void Init();
	void Destroy();
	void Snap();
	static void callback_connect(char* camera_serial_number, enum TL_CAMERA_USB_PORT_TYPE usb_bus_speed, void* context);
	static void callback_disconnect(char* camera_serial_number, void* context);
	void frame_available_callback(void* sender, unsigned short* image_buffer, int frame_count, unsigned char* metadata, int metadata_size_in_bytes);
	tira::image<unsigned char> Image8();
	tira::image<unsigned short> Image16();
	//void SaveColormap(std::string filename);
};