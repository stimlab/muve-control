#include "ThorlabCamera.h"
#include "ThorlabStage.h"

#include <filesystem>

struct MUVE_ImageData {
	double x;								// x position of the image (in millimeters)
	double y;								// y position of the image (in millimeters)
	size_t index_x;
	size_t index_y;
	tira::image<unsigned char> image;
};

struct MUVE_FrameCommand {						// stores command data for an automated MUVE imaging command
	double x;
	double y;
	size_t index_x;
	size_t index_y;
};

struct MUVE_ScanCommand {						// stores the command for scanning a mosaic image (and cutting if instructed)
	double x;
	double y;
	bool cut;
};

class MUVE_Microscope {
	double m_scan_start[2];							// starting (x,y) coordinates for a mosaic scan
	//double m_scan_size[2];							// size of the mosaic scan along x and y
	double m_fov[2];								// field of view of the current objective
	double m_overlap;								// overlap between sections
	double m_stage_limits[3];						// x,y,z limits for the stage

	size_t m_current_index[2];
	size_t m_scan_frames[2];						// size of the mosaic (in images) being collected

	// instrumentation parameters
	double m_objective_x;							// x-axis position that places the block under the objective
	double m_knife_x;								// x-axis position that places the block under the knife
	double m_cut_axis_y;							// y-axis position used for cutting
	double m_cut_plane_z;							// z-axis of the cutting plane (where the knife is cutting and the objective is in focus)
	double m_cut_length;							// length of the cut
	double m_max_velocity[3];						// maximum stage velocity
	double m_max_acceleration[3];					// maximum stage acceleration
	double m_cut_velocity_x;						// cutting velocity of the x axis
	double m_sample_clearance_mm;					// small clearance value between the knife and sample surface during the backswing

	// cutting parameters
	double m_section_thickness_um = 1;					// thickness of a section (in micrometers)
	double m_sample_thickness_mm = 3;					// thickness of the sample (in millimeters)

	std::deque<MUVE_FrameCommand> m_frames_in_progress;								// queue stores images that are in the process of being collected images
	std::deque<MUVE_ImageData> m_frames_collected;									// queue stores images that have been collected
	enum ImagingStatus {Positioning, Imaging, Idle} m_imaging_status;

	std::deque<MUVE_ScanCommand> m_scan_queue;			// queue stores images to be scanned (or cut)
	bool m_scanning;
	std::string m_image_directory;
	unsigned int m_num_scans;						// number of image sections collected and saved since the last directory change

	void m_UpdateMosaicSize(double sx, double sy);
	void m_IndexToPosition(size_t xi, size_t yi, double& x, double& y);
	void m_ProcessFramesQueue();
	void m_SaveFrames();
	void m_ClearFrames();
public:
	ThorlabStage stage;
	ThorlabCamera camera;

	MUVE_Microscope();
	void Init();
	void Destroy();
	bool isActive();
	void PurgeScans();

	// move commands
	void toObjective();
	void toKnife();
	
	// image acquisition functions
	void AcquireImage(size_t xi = 0, size_t yi = 0);														// acquire an image at the current x,y location
	void QueueMosaicFrames();										// queues the acquisition of an image at a specified x,y location
	void QueueScan(double x, double y, bool cut = false);											// queues a scan
	void QueueScan(bool cut = false);
	bool PopImage(MUVE_ImageData& image);
	size_t getImageQueueSize();															// query the number of image currently in the queue
	MUVE_ImageData getCollectedImage(size_t index);									// return the image in the image queue associated with index
	size_t getCommandQueueSize();														// query the number of commands currently in the stage command queue
	void ProcessCommandQueue();															// process MUVE activity

	//void setScan(double startx, double starty, double sizex, double sizey);
	void setScanStart(double x, double y);
	void setScanSize(double x, double y);
	void setScanFrames(unsigned int x, unsigned int y);
	void setSectionThickness(double thickness_um);
	void setSampleThickness(double thickness_mm);
	void setFov(double fov_x, double fov_y);
	void setOverlapPercent(double overlap);
	void setPathParameters(double objective_x, double knife_x, double cut_y, double cut_plane_z, double cut_length);
	void setMaxVelocity(double x, double y, double z);
	void setMaxAcceleration(double x, double y, double z);
	void setCutVelocity(double cut_velocity);
	void setExposure(double ms);


	void getPathParameters(double& objective_x, double& knife_x, double& cut_y, double& cut_plane_z, double& cut_length);
	void getStageLimits(double& x_min, double& x_max, double& y_min, double& y_max, double& z_min, double& z_max);
	void getMaxVelocity(double& x, double& y, double& z);
	void getMaxAcceleration(double& x, double& y, double& z);
	double getCutVelocity();
	void getSampleThickness(double& thickness_mm);
	void getFov(double& fov_x, double& fov_y);
	double getOverlapPercent();
	void getScanFrames(unsigned int& nx, unsigned int& ny);
	void getScanSize(double& x, double& y);
	void getScanStart(double& x, double& y);
	void setImageDirectory(std::string dir);
	double getExposure();

	void Cut();
	void QueueMove(int axis, double position);
	void QueueMove(int axis, double position, double velocity);
	void QueueMove(int axis, double position, double velocity, double acceleration);
	void ClearImages();

};