#include "ThorlabCamera.h"
#include <iostream>
#include "thorcam/tl_camera_sdk_load.h"
#include "thorcam/tl_camera_sdk.h"
#include "thorcam/tl_mono_to_color_processing_load.h"
#include "thorcam/tl_mono_to_color_processing.h"

void frame_available_callback_global(void* sender, unsigned short* image_buffer, int frame_count, unsigned char* metadata, int metadata_size_in_bytes, void* context) {
	((ThorlabCamera*)context)->frame_available_callback(sender, image_buffer, frame_count, metadata, metadata_size_in_bytes);
}

void ThorlabCamera::callback_connect(char* camera_serial_number, enum TL_CAMERA_USB_PORT_TYPE usb_bus_speed, void* context) {
	std::cout << "camera " << camera_serial_number << " connected with bus speed = " << usb_bus_speed << std::endl;
}

void ThorlabCamera::callback_disconnect(char* camera_serial_number, void* context) {
	std::cout << "camera " << camera_serial_number << " disconnected!" << std::endl;
}

// The callback that is registered with the camera
void ThorlabCamera::frame_available_callback(void* sender, unsigned short* image_buffer, int frame_count, unsigned char* metadata, int metadata_size_in_bytes) {
	if (m_is_first_frame_finished)
		return;

	std::cout << "image buffer = 0x" << image_buffer << std::endl;
	std::cout << "frame_count = " << frame_count << std::endl;
	std::cout << "meta data buffer = 0x" << metadata << std::endl;
	std::cout << "metadata size in bytes = " << metadata_size_in_bytes << std::endl;

	m_is_first_frame_finished = 1;

	// If you need to save the image data for application specific purposes, this would be the place to copy it into separate buffer.
	if (m_callback_buffer)
		memcpy(m_callback_buffer, image_buffer, (sizeof(unsigned short) * m_image_size[0] * m_image_size[1]));

	SetEvent(m_frame_acquired_event);

}



void ThorlabCamera::allocate_buffers() {
	m_callback_buffer = (unsigned short*)malloc(sizeof(unsigned short) * m_image_size[0] * m_image_size[1]);

	// color image size will be 3x the size of a mono image
	//m_buffer = (unsigned short*)malloc(sizeof(unsigned short) * m_image_size[0] * m_image_size[1] * 3);
	//m_image = tira::image<short>(m_image_size[0], m_image_size[1], 3);
}

ThorlabCamera::ThorlabCamera() {
	m_image_size[0] = 256;
	m_image_size[1] = 256;
	m_exposure = 300000;						// 300 ms
}

void ThorlabCamera::setExposure(double milliseconds) {
	m_exposure = (long long)(milliseconds * 1000);
	Init();
}

double ThorlabCamera::getExposure() {
	return (double)m_exposure / 1000.0;
}

void ThorlabCamera::Init() {
	if (tl_camera_sdk_dll_initialize()) {
		std::cout << "ERROR: Failed to initialize ThorCam DLL" << std::endl;
		return;
	}
	m_dll_init = true;

	if (tl_camera_open_sdk()) {
		std::cout << "ERROR: Failed to open ThorCam SDK" << std::endl;
		return;
	}
	m_sdk_open = true;

	char camera_ids[1024];
	camera_ids[0] = 0;

	// Discover cameras.
	if (tl_camera_discover_available_cameras(camera_ids, 1024)) {
		std::cout << "ERROR: Failed to discover cameras - " << tl_camera_get_last_error() << std::endl;
		return;
	}
	if (!strlen(camera_ids)) {
		std::cout << "No cameras found." << std::endl;
		return;
	}
	std::cout << "Camera IDs: " << camera_ids << std::endl;

	// Camera IDs are separated by spaces.
	char* p_space = strchr(camera_ids, ' ');
	if (p_space)
		*p_space = '\0'; // isolate the first detected camera
	char first_camera[256];
	strcpy_s(first_camera, 256, camera_ids);
	std::cout << "Opening camera " << first_camera << "...";
	// Connect to the camera (get a handle to it).
	if (tl_camera_open_camera(first_camera, &m_camera_handle)) {
		std::cout << std::endl<< "ERROR: Failed to discover cameras - " << tl_camera_get_last_error() << std::endl;
		return;
	}

	std::cout << "handle = 0x" << m_camera_handle << std::endl;

	if (tl_mono_to_color_processing_initialize()) {
		std::cout << "ERROR: Failed to open mono/color SDK" << std::endl;
		return;
	}
	m_mono2color_sdk_open = true;

	tl_camera_get_camera_sensor_type(m_camera_handle, &m_camera_sensor_type);
	tl_camera_get_color_filter_array_phase(m_camera_handle, &m_color_filter_array_phase);
	tl_camera_get_color_correction_matrix(m_camera_handle, m_color_correction_matrix);
	tl_camera_get_default_white_balance_matrix(m_camera_handle, m_default_white_balance_matrix);
	tl_camera_get_bit_depth(m_camera_handle, &m_bit_depth);
	tl_mono_to_color_create_mono_to_color_processor(
		m_camera_sensor_type,
		m_color_filter_array_phase,
		m_color_correction_matrix,
		m_default_white_balance_matrix,
		m_bit_depth,
		&m_mono_to_color_processor_handle);
	
	tl_camera_set_camera_connect_callback(callback_connect, 0);
	tl_camera_set_camera_disconnect_callback(callback_disconnect, 0);


	tl_camera_set_exposure_time(m_camera_handle, m_exposure);
	tl_camera_get_gain_range(m_camera_handle, &m_gain_range[0], &m_gain_range[1]);
	if (m_gain_range[1] > 0) {																// if this camera supports gain, set it to 6.0 decibels
		
		int gain_index;
		tl_camera_convert_decibels_to_gain(m_camera_handle, m_gain_dB, &gain_index);		// set the default gain
		tl_camera_set_gain(m_camera_handle, gain_index);
	}

	tl_camera_set_frames_per_trigger_zero_for_unlimited(m_camera_handle, 1);				// collect one frame per trigger
	tl_camera_set_frame_available_callback(m_camera_handle, frame_available_callback_global, this);		// set the camera callback
	//tl_camera_arm(m_camera_handle, 1);														// buffer one frame
	tl_camera_get_image_width(m_camera_handle, &m_image_size[0]);							// get the current camera image width and height
	tl_camera_get_image_height(m_camera_handle, &m_image_size[1]);

	allocate_buffers();																		// allocate space for the image buffers
}

void ThorlabCamera::Destroy() {
	if (m_camera_handle) {
		if (tl_camera_close_camera(m_camera_handle)) {
			std::cout << "ERROR: Failed to close camera - " << tl_camera_get_last_error() << std::endl;
		}
		m_camera_handle = 0;
	}
	if (m_sdk_open) {
		if (tl_camera_close_sdk()) {
			std::cout << "ERROR: Failed to close ThorCam SDK" << std::endl;
		}
		m_sdk_open = false;
	}
	if (m_dll_init){
		if (tl_camera_sdk_dll_terminate()){
			std::cout << "ERROR: Failed to close ThorCam DLL" << std::endl;
		}
		m_dll_init = false;
	}
	if (m_camera_handle) {
		tl_camera_disarm(m_camera_handle);
	}
}

/// <summary>
/// Launches a thread to acquire an image from the camera.
/// </summary>
void ThorlabCamera::Snap() {
	m_is_first_frame_finished = 0;
	tl_camera_arm(m_camera_handle, 1);														// buffer one frame
	if (m_camera_handle) {
		tl_camera_issue_software_trigger(m_camera_handle);

		for (;;) {
			WaitForSingleObject(m_frame_acquired_event, INFINITE);
			if (m_is_first_frame_finished) break;
		}
	}
	tl_camera_disarm(m_camera_handle);
}

tira::image<unsigned char> ThorlabCamera::Image8() {
	tira::image<unsigned char> img8(m_image_size[0], m_image_size[1], 3);
	if (m_camera_handle) {														// If there is an active camera
		tl_mono_to_color_transform_to_24(m_mono_to_color_processor_handle,		// transform the buffer into a color image
			m_callback_buffer,
			m_image_size[0],
			m_image_size[1],
			img8.data());
	}
	else {																		// Otherwise fill the texture with a simple color gradient (for debugging)
		for (size_t yi = 0; yi < m_image_size[1]; yi++) {
			for (size_t xi = 0; xi < m_image_size[0]; xi++) {
				img8(xi, yi, 0) = (unsigned char)((double)xi / (double)m_image_size[0] * 256);
				img8(xi, yi, 1) = (unsigned char)((double)yi / (double)m_image_size[1] * 256);
				img8(xi, yi, 2) = 128;
			}
		}
	}
	return img8;
}

tira::image<unsigned short> ThorlabCamera::Image16() {
	tira::image<unsigned short> img16(m_image_size[0], m_image_size[1], 3);
	if (m_camera_handle) {
		tl_mono_to_color_transform_to_48(m_mono_to_color_processor_handle,
			m_callback_buffer,
			m_image_size[0],
			m_image_size[1],
			img16.data());
	}
	return img16;
}

