#include "muve.h"


/// <summary>
/// Default constructor sets normal limits on the stage parameters
/// </summary>
MUVE_Microscope::MUVE_Microscope() {

	// set hardware limits
	m_stage_limits[0] = 150;
	m_stage_limits[1] = 150;
	m_stage_limits[2] = 100;

	// set velocity limits
	setMaxVelocity(20, 20, 20);

	// set acceleration limits
	setMaxAcceleration(30, 30, 30);


	m_cut_velocity_x = 5;
	m_sample_clearance_mm = 1;

	m_cut_axis_y = 77;
	m_cut_plane_z = 80;
	m_knife_x = 88;
	m_cut_length = 42;
	m_objective_x = 20;

	m_sample_thickness_mm = 2.383;

	//m_scan_frames[0] = 1;						// default to a 1x1 mosaic (one camera FOV)
	//m_scan_frames[1] = 1;
	m_current_index[0] = 0;
	m_current_index[1] = 0;

	setScanStart(0.0, 0.0);
	setFov(1.0, 0.5);
	setScanSize(10.0, 10.0);
	
	m_imaging_status = ImagingStatus::Idle;
	m_scanning = false;
}

bool MUVE_Microscope::isActive() {
	if (stage.isMoving()) return true;

	return false;
}

void MUVE_Microscope::toObjective() {
	QueueMove(3, m_cut_plane_z + m_sample_thickness_mm);
	QueueMove(2, m_cut_axis_y);
	QueueMove(1, m_objective_x);
}

void MUVE_Microscope::toKnife() {
	QueueMove(3, m_cut_plane_z + m_sample_thickness_mm);
	QueueMove(2, m_cut_axis_y);
	QueueMove(1, m_knife_x);
}

void MUVE_Microscope::setPathParameters(double objective_x, double knife_x, double cut_y, double cut_plane_z, double cut_length) {
	if (objective_x >= 0 && objective_x <= m_stage_limits[0])
		m_objective_x = objective_x;

	if (knife_x >= 0 && knife_x <= m_stage_limits[0])
		m_knife_x = knife_x;

	if (cut_y > 0 && cut_y <= m_stage_limits[1])
		m_cut_axis_y = cut_y;

	if (cut_length > 0 && cut_length + objective_x <= m_stage_limits[0])
		m_cut_length = cut_length;

	if (cut_plane_z >= 0 && cut_plane_z <= m_stage_limits[2])
		m_cut_plane_z = cut_plane_z;
}

void MUVE_Microscope::setCutVelocity(double cut_velocity) {
	m_cut_velocity_x = cut_velocity;
}

void MUVE_Microscope::setMaxVelocity(double x, double y, double z) {
	m_max_velocity[0] = x;
	m_max_velocity[1] = y;
	m_max_velocity[2] = z;
}

void MUVE_Microscope::setMaxAcceleration(double x, double y, double z) {
	m_max_acceleration[0] = x;
	m_max_acceleration[1] = y;
	m_max_acceleration[2] = z;

}

void MUVE_Microscope::getPathParameters(double& objective_x, double& knife_x, double& cut_y, double& cut_plane_z, double& cut_length) {
	objective_x = m_objective_x;
	knife_x = m_knife_x;
	cut_y = m_cut_axis_y;
	cut_length = m_cut_length;
	cut_plane_z = m_cut_plane_z;
}

double MUVE_Microscope::getCutVelocity() {
	return m_cut_velocity_x;
}
void MUVE_Microscope::getStageLimits(double& x_min, double& x_max, double& y_min, double& y_max, double& z_min, double& z_max) {
	x_min = 0.0;
	x_max = m_stage_limits[0];
	y_min = 0.0;
	y_max = m_stage_limits[1];
	z_min = 0.0;
	z_max = m_stage_limits[2];
}

void MUVE_Microscope::getMaxVelocity(double& x, double& y, double& z) {
	x = m_max_velocity[0];
	y = m_max_velocity[1];
	z = m_max_velocity[2];
}

void MUVE_Microscope::getMaxAcceleration(double& x, double& y, double& z) {
	x = m_max_acceleration[0];
	y = m_max_acceleration[1];
	z = m_max_acceleration[2];
}

void MUVE_Microscope::getSampleThickness(double& thickness_mm) {
	thickness_mm = m_sample_thickness_mm;
}

void MUVE_Microscope::getFov(double& fov_x, double& fov_y) {
	fov_x = m_fov[0];
	fov_y = m_fov[1];
}

double MUVE_Microscope::getOverlapPercent() {
	return m_overlap * 100;
}

void MUVE_Microscope::getScanFrames(unsigned int& nx, unsigned int& ny) {
	nx = m_scan_frames[0];
	ny = m_scan_frames[1];
}

void MUVE_Microscope::getScanSize(double& x, double& y) {
	double x_step = m_fov[0] * (1 - m_overlap);
	double y_step = m_fov[1] * (1 - m_overlap);

	x = x_step * m_scan_frames[0];
	y = y_step * m_scan_frames[1];

}

void MUVE_Microscope::getScanStart(double& x, double& y) {
	x = m_scan_start[0];
	y = m_scan_start[1];
}

void MUVE_Microscope::m_UpdateMosaicSize(double sx, double sy) {
	double adjusted_fov[2];
	adjusted_fov[0] = m_fov[0] * (1.0 - m_overlap);									// calculate the adjusted fov accounting for overlap between images
	adjusted_fov[1] = m_fov[1] * (1.0 - m_overlap);

	m_scan_frames[0] = (unsigned int)ceil(sx / adjusted_fov[0]);		// Calculate the number of images in the mosaic along x
	m_scan_frames[1] = (unsigned int)ceil(sy / adjusted_fov[1]);		// and the number of images in the mosaic along y.
}

void MUVE_Microscope::m_IndexToPosition(size_t xi, size_t yi, double& x, double& y) {
	double adjusted_fov[2];
	adjusted_fov[0] = m_fov[0] * (1.0 - m_overlap);									// calculate the adjusted fov accounting for overlap between images
	adjusted_fov[1] = m_fov[1] * (1.0 - m_overlap);

	x = xi * adjusted_fov[0] + m_scan_start[0];
	y = yi * adjusted_fov[1] + m_scan_start[1];
}

void MUVE_Microscope::Init() {
	camera.Init();
	stage.Init();

	double stored_val;
	stored_val = stage.getAcceleration(1);
	if (stored_val < m_max_acceleration[0]) m_max_acceleration[0] = stored_val;
	stored_val = stage.getAcceleration(2);
	if (stored_val < m_max_acceleration[1]) m_max_acceleration[1] = stored_val;
	stored_val = stage.getAcceleration(3);
	if (stored_val < m_max_acceleration[2]) m_max_acceleration[2] = stored_val;


	stored_val = stage.getVelocity(1);
	if (stored_val < m_max_velocity[0]) m_max_velocity[0] = stored_val;
	stored_val = stage.getVelocity(2);
	if (stored_val < m_max_velocity[1]) m_max_velocity[1] = stored_val;
	stored_val = stage.getVelocity(3);
	if (stored_val < m_max_velocity[2]) m_max_velocity[2] = stored_val;

	stage.StartLog("stagelog.txt");
}

void MUVE_Microscope::Destroy() {
	camera.Destroy();
	stage.Destroy();
}

void MUVE_Microscope::setScanStart(double x, double y) {
	m_scan_start[0] = x;
	m_scan_start[1] = y;
}

void MUVE_Microscope::setScanSize(double x, double y) {
	m_UpdateMosaicSize(x, y);											// update the mosaic size
}

void MUVE_Microscope::setScanFrames(unsigned int x, unsigned int y) {
	m_scan_frames[0] = x;
	m_scan_frames[1] = y;
}

void MUVE_Microscope::setFov(double fov_x, double fov_y) {
	if(fov_x > 0.0)
		m_fov[0] = fov_x;
	if(fov_y > 0.0)
		m_fov[1] = fov_y;
	

	//m_UpdateMosaicSize(size_x, size_y);									// update the mosaic size
}

void MUVE_Microscope::setExposure(double ms) {
	camera.setExposure(ms);
}

double MUVE_Microscope::getExposure() {
	return camera.getExposure();
}

void MUVE_Microscope::setOverlapPercent(double overlap) {
	if (overlap >= 0 && overlap < 100)
		m_overlap = overlap * 0.01;
}


/// <summary>
/// Acquires an image at the current stage position
/// </summary>
void MUVE_Microscope::AcquireImage(size_t xi, size_t yi) {
	camera.Snap();										// Send the command to acquire an image from the camera
	MUVE_ImageData new_image;							// Create a new image data structure (stores position)
	new_image.index_x = xi;								
	new_image.index_y = yi;
	new_image.x = stage.getPosition(1);					// Get the position of the image
	new_image.y = stage.getPosition(2);
	new_image.image = camera.Image8();					// Acquire an image from the camera
	m_frames_collected.push_back(new_image);
}

/// <summary>
/// Queue the collection of a mosaic at the specified (x,y) coordinate
/// </summary>
/// <param name="x"></param>
/// <param name="y"></param>
void MUVE_Microscope::QueueMosaicFrames() {
	double x_offset, y_offset;
	for (int iy = 0; iy < m_scan_frames[1]; iy++) {
		for (int ix = 0; ix < m_scan_frames[0]; ix++) {
			m_IndexToPosition(ix, iy, x_offset, y_offset);
			MUVE_FrameCommand new_command;
			new_command.index_x = ix;
			new_command.index_y = iy;
			new_command.x = x_offset;
			new_command.y = y_offset;
			m_frames_in_progress.push_back(new_command);
		}
	}
	
}

void MUVE_Microscope::QueueScan(double x, double y, bool cut) {
	MUVE_ScanCommand new_scan;
	new_scan.x = x;
	new_scan.y = y;
	new_scan.cut = cut;
	m_scan_queue.push_back(new_scan);
}

/// <summary>
/// Removes all pending scans and sections from the queue
/// </summary>
void MUVE_Microscope::PurgeScans() {
	m_scan_queue.clear();
}

void MUVE_Microscope::QueueScan(bool cut) {
	QueueScan(m_scan_start[0], m_scan_start[1], cut);
}

MUVE_ImageData MUVE_Microscope::getCollectedImage(size_t index) {
	return m_frames_collected[index];
}

void MUVE_Microscope::ClearImages() {
	m_ClearFrames();
}

void MUVE_Microscope::m_ProcessFramesQueue() {
	// If the imaging status is Idle and there are frames in progress, move the stage and change the status
	if (m_imaging_status == ImagingStatus::Idle &&
		m_frames_in_progress.size() != 0) {
		m_imaging_status = ImagingStatus::Positioning;
		QueueMove(1, m_frames_in_progress.front().x);
		QueueMove(2, m_frames_in_progress.front().y);
		return;
	}

	// If the imaging status is Positioning, then we have arrived at the correct position (barrier clause ensures this)
	// Begin acquiring the image and change the status
	if (m_imaging_status == ImagingStatus::Positioning) {
		m_imaging_status = ImagingStatus::Imaging;
		AcquireImage(m_frames_in_progress.front().index_x, m_frames_in_progress.front().index_y);
		m_frames_in_progress.pop_front();
		m_imaging_status = ImagingStatus::Idle;
		return;
	}
}

void MUVE_Microscope::ProcessCommandQueue() {

	stage.UpdateStatus();										// update the stage status

	// Test conditions for performing any actions
	
	if (stage.getCommandQueueSize() > 0) return;				// there must not be any pending commands in the stage queue
	if (stage.isMoving()) return;								// the stage must not be moving

	if (m_frames_in_progress.size()) {							// if there are currently frames in the queue
		m_ProcessFramesQueue();									// process them
		return;
	}

	// THE STAGE IS IDLE AND THERE ARE NO MOSAICS BEING PROCESSED
	if (m_scan_queue.size() == 0) return;						// if there are no commands in the scan queue, return

	// THE STAGE IS IDLE AND THERE IS SCANNING TO BE DONE
	if (!m_scanning) {															// if the MUVE system is not actively scanning
		QueueMosaicFrames();													// queue the frames for scanning
		m_scanning = true;														// flag the system as currently scanning
		return;		
	}	
		
	// A SCAN IS IN PROCESS BUT ALL OF THE IMAGING IS DONE
	m_SaveFrames();
	m_num_scans++;
	if (m_scan_queue.front().cut) Cut();					// if the current front of the scan queue requires cutting, queue the cut
	m_scan_queue.pop_front();
	
	m_scanning = false;

}

bool MUVE_Microscope::PopImage(MUVE_ImageData& image) {
	if (m_frames_collected.empty()) return false;

	image = m_frames_collected.front();
	m_frames_collected.pop_front();
	return true;
}

size_t MUVE_Microscope::getImageQueueSize() {
	return m_frames_collected.size();
}

size_t MUVE_Microscope::getCommandQueueSize() {
	return stage.getCommandQueueSize();
}

void MUVE_Microscope::Cut() {
	m_sample_thickness_mm -= m_section_thickness_um * 0.001;

	toKnife();																								// move to the knife position
	stage.QueueMove(3, m_cut_plane_z + m_sample_thickness_mm, m_max_velocity[2], m_max_acceleration[2]);    // move the sample up by the section thickness    
	stage.QueueMove(1, m_knife_x + m_cut_length, m_cut_velocity_x, m_max_acceleration[0]);					// queue the move to the cut position
	stage.QueueMove(3, m_cut_plane_z + m_sample_thickness_mm + m_sample_clearance_mm, m_max_velocity[2], m_max_acceleration[2]);	// move the sample down slightly by the specified clearance distance
	stage.QueueMove(1, m_knife_x, m_max_velocity[0], m_max_acceleration[0]);								// queue the move back to the objective position
	stage.QueueMove(3, m_cut_plane_z + m_sample_thickness_mm, m_max_velocity[2], m_max_acceleration[2]);	// move the sample down slightly by the specified clearance distance

	//stage.setVelocity(1, m_max_velocity[0]);		// set the stage velocity to the standard (fast)
}

void MUVE_Microscope::QueueMove(int axis, double position, double velocity, double acceleration) {
	stage.QueueMove(axis, position, velocity, acceleration);
}

void MUVE_Microscope::QueueMove(int axis, double position, double velocity) {
	stage.QueueMove(axis, position, velocity, m_max_acceleration[axis-1]);
}

void MUVE_Microscope::QueueMove(int axis, double position) {
	stage.QueueMove(axis, position, m_max_velocity[axis-1], m_max_acceleration[axis - 1]);
}

void MUVE_Microscope::setSectionThickness(double thickness_um) {
	m_section_thickness_um = thickness_um;
}

void MUVE_Microscope::setSampleThickness(double thickness_mm) {
	m_sample_thickness_mm = thickness_mm;
}

void MUVE_Microscope::setImageDirectory(std::string dir) {
	m_image_directory = dir;
	m_num_scans = 0;
}

/// <summary>
/// Saves the captured images to the destination directory
/// </summary>
/// <param name="directory"></param>
void MUVE_Microscope::m_SaveFrames() {
	if (m_image_directory == "") {									// if no directory is provided, don't save anything
		m_ClearFrames();
		return;
	}

	std::filesystem::path dest_path(m_image_directory);				// create a path object for the destination directory

	if (!std::filesystem::exists(dest_path)) {				// if the path doesn't exist, create it
		std::error_code ec;
		std::filesystem::create_directory(dest_path, ec);
		std::cout << "Repo Directory Creation: " << ec.message() << std::endl;
	}

	for (int i = 0; i < m_frames_collected.size(); i++) {
		size_t xi = m_frames_collected[i].index_x;
		size_t yi = m_frames_collected[i].index_y;
		std::stringstream directory_ss;
		directory_ss << std::setw(3) << std::setfill('0') << xi << "_" << std::setw(3) << std::setfill('0') << yi;
		std::filesystem::path image_path(m_image_directory + "\\" + directory_ss.str());
		if (!std::filesystem::exists(image_path)) {									// if the frame path doesn't exist
			std::error_code ec;																// create it
			std::filesystem::create_directory(image_path, ec);
			std::cout << "Frame Directory Creation: " << ec.message() << std::endl;
		}
		std::stringstream filename_ss;
		filename_ss << std::setw(5) << std::setfill('0') << m_num_scans<<".bmp";
		m_frames_collected[i].image.save(m_image_directory + "\\" + directory_ss.str() + "\\" + filename_ss.str());
	}
	m_ClearFrames();
}

void MUVE_Microscope::m_ClearFrames() {
	m_frames_collected.clear();
}