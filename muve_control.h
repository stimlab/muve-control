#pragma once

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "tira/graphics_gl.h"

struct MuveImageTexture {
    double pos[2] = { 0, 0 };
    tira::glTexture tex;
};

void InitGUI(GLFWwindow* window, float ui_scale, const char* glsl_version);
void RenderGUI();
void DestroyGUI();